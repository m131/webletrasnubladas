const ALGOLIA_APP_ID = "X9DTO4T8OB";
const ALGOLIA_SEARCH_API_KEY = "7af3a9566cce4d8fe030e1254924c649";
let firstLoad = true;

const algoliaClient = algoliasearch(
  ALGOLIA_APP_ID,
  ALGOLIA_SEARCH_API_KEY
);

const searchClient = {
  ...algoliaClient,
  search(requests) {
    if (firstLoad) {
      firstLoad = false;
      return
    }

    return algoliaClient.search(requests)
  }
}

const search = instantsearch({
  indexName: "letrasnubladas",
  searchClient: searchClient
});

search.addWidgets([
  instantsearch.widgets.searchBox({
    container: "#searchbox",
  }),

  instantsearch.widgets.hits({
    container: "#hits",
    templates: {
      item: `
        <article>
          <h3><a href="{{url}}">{{title}}</a></h3>
          <p>
            <small>
              {{#helpers.highlight}}
                {"attribute": "content"}
              {{/helpers.highlight}}
            </small>
          </p>
        </article>
      `
    }
  }),
]);

search.start();
