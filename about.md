---
layout: page
title: Acerca de Letras Nubladas y Mario Flores Abreu
image: /assets/pages-images/guitar-case.jpg
---
# Mario Flores Abreu

Escribo por placer, por amor, por tristeza, porque me gusta, por cualquier sentimiento que se me atraviese. Si te ha gustado algo del sitio, puedes tomarlo libremente siempre y cuando incluyas la referencia del sitio y el autor. También puedes **[DONAR](https://www.paypal.com/donate?hosted_button_id=NZ9Z8YDHSMMEC)** una aportación económica (por pequeña que sea), ello me ayudará a estar más tranquilo y a escribir más seguido. **Puedes contactarme por [Twitter](https://twitter.com/maxito_abreu)**.

Imagen por: [Pixabay](https://pixabay.com/photos/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1023340)
