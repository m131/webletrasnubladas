---
layout: post
title: Brazos vacíos
excerpt: Tengo los brazos vacíos, las pisadas solitarias, los suspiros no se dirigen a ningún lugar. Los ojos anegados y bien abiertos están centrados en la luna
author: Mario
category: poema
---

Tengo los brazos vacíos, las pisadas solitarias, los suspiros no se dirigen a ningún lugar. Los ojos anegados y bien abiertos están centrados en la luna, observando su calma, su lento andar por el oscuro mar estrellado, no hay nada más allá, ni aquí, nada que no sea la nada, el abandono, no hay nada más que una mirada anegada y una garganta dispuesta a aullar a la luna el vacío de la soledad. 
