---
layout: post
title: Abrir los ojos
excerpt: Cerró sus ojos frente al viento, suspiró entendiendo que la vida misma son los sueños.
author: Mario
category: micro cuento
---

Cerró sus ojos frente al viento y suspiró profundamente, entonces entendió que muchas veces los
sueños son sólo ilusiones pasajeras o metas inalcanzables, y que la vida misma son los sueños,
las metas y los anhelos que inhalamos cada amanecer, al despertar, levantarnos y abrir los ojos al mundo.
