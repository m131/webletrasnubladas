---
layout: post
title: Cuando vengas
excerpt: Cuándo vengas, cuando salgas de la nada que nos separa y se me revele tu nombre al fin.
author: Mario
category: poema
---

Cuándo vengas, cuando salgas de la nada que nos separa y se me revele tu nombre al fin, cuando las miradas se crucen y se junten nuestras palmas y antes de nada de mi boca escucharás: no seré sombra ni grillete y no seré amo ni dueño; seré viento y libertad en tus alas para volar. Seré cómplice y compañero, para que juntos, vayamos de la mano a donde tengamos que llegar.
