---
layout: post
title: Arrepentimiento
excerpt: No existe, de ninguna manera, forma alguna de vivir de tal modo que en el último aliento no te arrepientas de algo. Siempre, absolutamente siempre habrá algo de que arrepentirte.
author: Mario
category: reflexión
---

No existe, de ninguna manera, forma alguna de vivir de tal modo que, en el último aliento no te arrepientas de algo. Siempre, absolutamente siempre habrá algo de que arrepentirse, sea grande o pequeño, una palabra, una caricia, un acto o la ausencia de ellos. No importa cuan buena pueda ser tu vida, te arrepentirás de algo indudablemente en algún momento de tu vida o al final de ella; te  arrepentirás de no decir un te quiero, de no tender una mano, de pretender ser quién no eres, te arrepentirás de dejar ir un amor, de retener otro, te arrepentirás de creer en evidentes mentiras aunque no te arrepientas del amor entregado.  Indudablemente no existe una vida sin arrepentimiento, lo único que resta es  tratar de hacer las cosas correcta para que, al final, los arrepentimientos sean pocos y no inmensurables.
