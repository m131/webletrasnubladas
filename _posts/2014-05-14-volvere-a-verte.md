---
layout: post
title: Volveré a verte
excerpt: Sé que volveré a verte, que alzaré mi mano desnuda de todo juicio  
author: Mario
category: poema
---

Sé que volveré a verte,  
que alzaré mi mano  
desnuda de todo juicio  
para acariciar tu rostro  
lavado de toda culpa,  
sé que mi suspiro  
moverá estrellas  
entre tus cabellos  
y que las miradas  
cruzarán el vacío;  
tú me llenarás  
de ti y yo esperaré  
no partas al amanecer.  
