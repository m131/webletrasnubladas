---
layout: post
title: Suspiro de Dios
excerpt: Hablo de tu belleza como algo invisible, algo que no puedo ver ni tocar, hablo de lo que siento al verte, de lo que me provocas en los ojos, en la piel, en mi calma
author: Mario
category: poema
---

Hablo de tu belleza como algo invisible, algo que no puedo ver ni tocar, hablo de lo que siento al verte, de lo que me provocas en los ojos, en la piel, en mi calma, esa belleza que no se puede describir ni con estas burdas palabras porque es algo que a simple vista no se puede admirar. Hablo de tus tibias y pequeñas manos que las siento sin tocarlas, de tu risa burlona que hace suspirar,  hablo de tu voz que dulce entra en los oídos pero se clava en el corazón. Hablo de esa belleza que ni tú puedes sentir, pero yo, yo la siento tan solo con pensarte, no como una imagen, no como una caricia, sino como un suspiro de Dios.
