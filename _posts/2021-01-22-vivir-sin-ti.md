---
layout: post
title: Vivir sin ti
excerpt: Puedo vivir sin ti, abocarme a mi soledad y quererme como te quiero a ti. Puedo andar sin ti, recorrer ríos, montes y valles sin imaginarte ahí, conmigo.
author: Mario
category: poema
---

Puedo vivir sin ti, abocarme a mi soledad y quererme como te quiero a ti. Puedo andar sin ti, recorrer ríos, montes y valles sin imaginarte ahí, conmigo. Puedo querer sin ti, porque he descubierto que puedo abrir y cerrar el corazón casi a voluntad con un poco de esfuerzo. Pero es justo en ese casi donde resides tú, porque la verdad es que prefiero vivir para mí y abocarme a ti, recorrer el mundo pensando en ti, y sobre todo, abrir el corazón para quererte a ti;  quererte sin necesitarte para vivir, pero quererte porque te quiero querer solo a ti. 
