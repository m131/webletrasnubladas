---
layout: post
title: No te aferres
excerpt: No te aferres a ilusiones, a espejismos sin razón a visiones de añoranza.  
author: Mario
category: poema
---

No te aferres a ilusiones,  
a espejismos sin razón  
a visiones de añoranza.  
  
No te prendas en llamas  
con minúsculas chispas  
que se pueden extinguir.  
  
No desnudes tu alma  
ante errantes vientos  
que se vuelven huracanes.   
  
Pero no pierdas la esperanza  
de nuevos amaneceres  
para desatar el alma.
