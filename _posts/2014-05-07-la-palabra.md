---
layout: post
title: La palabra
excerpt: La palabra que emanaba de su boca...
author: Mario
category: poema
---

La palabra que emanaba de su boca  
era como la chispa del pedernal,  
que enciende la ardiente hoguera  
para confortar y regocijar al cuerpo  
frío y entumecido que precisa de ella.	
