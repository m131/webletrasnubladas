---
layout: post
title: Salir a caminar
excerpt: He sabido que las personas para conocerse gustan de invitarse al restaurante, al cine ¿pero que clases de invitaciones son esas?
author: Mario
category: poema
---

He sabido que las personas para conocerse gustan de invitarse al restaurante, al cine ¿pero qué clases de invitaciones son esas? Yo no quiero estar sentado junto a ti escuchando otros murmullos que no sean los tuyos, no quiero fijar mi vista en una pantalla gigante, yo la quiero fijar en tus ojos, en tu mirada. 

Lo que quiero es invitarte a caminar y tener una platica sin sentido y luego de la nada volverla trascendental, sentarnos bajo algún árbol viejo y suponer lo que ha visto, lo que ha vivido; reír un rato de los demás al imitarlos al verlos pasar, juntos brincar charcos tan solo porque sí, y al final del día, mirarte a los ojos e invitarte a volar.
