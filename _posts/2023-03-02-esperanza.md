---
layout: post
title: Esperanza
excerpt: Siempre hay un atisbo de adusta esperanza en lo más profundo de nuestras causas pérdidas, una que se aferra a lo imposible
author: Mario
category: poema
---

Siempre hay un atisbo de adusta esperanza en lo más profundo de nuestras causas
pérdidas, una que se aferra a lo imposible de la más absoluta negación. Siempre
hay un pedacito de tonta esperanza que nos mantiene vivos, en la tempestuosa
melancolía de lo que para siempre se perdió.
