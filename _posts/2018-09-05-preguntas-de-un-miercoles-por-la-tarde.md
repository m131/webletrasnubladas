---
layout: post
title: Preguntas de un miércoles por la tarde
excerpt: ¿Cuál es el sentido de la humanidad? ¿Cuál es el sentido de la vida humana?
author: Mario
category: reflexión
image:  /assets/post-images/night-photograph.jpg
---

¿Cuál es el sentido de la humanidad? ¿Cuál es el sentido de la vida humana? ¿Cuál es el sentido de la vida en general? ¿Complacer una deidad? ¿Complacer al universo, a la naturaleza? ¿Para que existe la vida si ésta terminará por extinguirse? ¿Cuál es el sentido de toda vida, cuál? Si al final somos un amasijo de materia inerte vuelta "vida" y que regresará a ser polvo estelar. 
