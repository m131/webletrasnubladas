---
layout: post
title: Romper los muros
excerpt: Voy a romper los muros y derribar los miedos, voy a sacar el corazón y explotar el alma
author: Mario
category: poema
---

Voy a romper los muros y derribar los miedos, voy a sacar el corazón y explotar el alma; 
voy por esta vez a jugarmela y tirar todas mis cartas; voy con todo el corazón a poner 
mi apuesta, porque gane o pierda, al fin se libera el alma.
