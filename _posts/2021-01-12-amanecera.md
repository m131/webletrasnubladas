---
layout: post
title: Amanecerá
excerpt: Amanecerá, pronto la luz dará color a las nubes y las aves trinarán y tú, tú no estás.
author: Mario
category: poema
---

Amanecerá, pronto la luz dará  
color a las nubes y las aves trinarán  
y tú, tú no estás.  
  
Al amanecer te llevo en el pensamiento  
pero no de la mano,  
no basta ya con pensarte.  
  
Quiero un amanecer  
con tu mano en la mía,  
con tu pupila como mi luz de día,  
y a ti, como el amanecer de mi vida.
