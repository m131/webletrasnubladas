---
layout: post
title: El día más importante
excerpt: El día más importante de mi vida será el día en que muera, que será mañana o tal vez en una semana o en muchos años. Pero sin duda será el día más importante
author: Mario
category: poema
---

El día más importante de mi vida será el día en que muera, que será mañana o tal vez en una semana o en muchos años. Pero sin duda será el día más importante, pues ese día ya nada podrá salir mal, no me preocuparé por un trabajo o dinero, mucho menos por morir en soledad como alguna vez sentí mientras dormía. 

Mi día más importante, sí, será cuando muera y ya no tenga que pensar en la veracidad o dudar de los consejos que me otorgan, tampoco me preocuparán las mentiras que me digan y que no sepa que lo son, mucho menos si la gente me engaña por creer en su palabra. Ese día no tendré que preocuparme en como vestirme, pues alguien más lo hará por mí y yo no tendré ni que elegir. 

Cuando ese día tan importante llegue (que podrá ser mañana, en una semana o en muchos años) ya no importará que tenga montones de poemas y cuentos inconclusos, mucho menos si hay quién los inspira o no; ya no importará si limpio o no la pecera, alguien más al fin lo hará. Pero cuando ese día suceda, ya no habrá hipotecas, ni recibos ni deudas. 

Porque el día más importante de mi existencia será cuando muera, porque de pronto para el mundo habré sido bueno y misteriosamente me querrán de la nada, y tendré cientos de flores que nunca nadie me procuro y siempre anhele. Ese día con suerte hasta seré amado, aunque tiempo después termine olvidado. 

Ese día tan importante ya no me preocuparé por estar cansado o desvelado escribiendo tonterías, tampoco si me duele la cabeza, el alma o las rodillas. 

Y ante la espera de que llegue esa importante fecha, solo una pregunta me llega ¿por qué todos esperan a que llegue ese día guardando palabras y sentimientos que podrían ser expresados, que podrían hacerle a alguien de verdad importante su día y su vida?

Porque postergamos y callamos como si fuéramos eternos, cuando en cualquier momento puede llegarnos el día más importante de nuestra vida. 






