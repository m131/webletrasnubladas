---
layout: post
title: Tender las manos
excerpt: Cuando la oscuridad se cierre sobre ti, cuando la llovizna se te vuelva tempestad, cuando no pares de temblar y caigas de rodillas, cuando el alma se haga ovillo en lo más recóndito de tu ser
author: Mario
category: poema
---

Cuando la oscuridad se cierre sobre ti, cuando la llovizna se te vuelva tempestad, cuando no pares de temblar y caigas de rodillas, cuando el alma se haga ovillo en lo más recóndito de tu ser, cuando la garganta se vuelva un nudo y los ojos se aneguen como lagunas, cuando sientas que no quieres ver otro amanecer, cuando quieras desaparecer, cuando los sueños se te hagan pesadillas ahí estaré, escondido en un rincón, sin que notes mi presencia pero con las manos dispuestas, sin decir palabra, sin apartar la mirada, para que en el momento que lo precises y lo digas, tenderte mis manos y mi ser, no pelearé por ti, no lucharé tus batallas, pero lo haré a tu lado, y aunque la piel se desgarre, aunque la valentía me falle y tenga el corazón en mano ensangrentado, no te dejaré caer, ahí estaré. 
