---
layout: post
title: Inmortalidad
excerpt: Te amé tanto que, un día, abandonó mi alma la cárcel de su cuerpo.
author: Mario
category: poema
---

>Te amé tanto que, un día, abandonó mi alma  
>la cárcel de su cuerpo. Errátil, y no hallándote,  
>regresó a la morada que yo daba por mía.  
>Mas no estaba mi cuerpo donde allí  
>lo dejara,  
>sino el tuyo, vastísimo, como un templo de oro.  
>Y no le diste asilo. Y ya no tendré muerte.  
>  
>-Vicente Núñez, Inmortalidad
