---
layout: post
title: No despertar
excerpt: De pronto, un lunes de madrugada, te colaste en mis sueños, y fue algo dulce e inesperado, hacía años que nadie venía a mi mientras mi cuerpo descansaba
author: Mario
category: poema
---

De pronto, un lunes de madrugada, te colaste en mis sueños, y fue algo dulce e inesperado, hacía años que nadie venía a mi mientras mi cuerpo descansaba. Viniste igual que un relámpago, súbitamente y de pronto iluminaste todo un largo instante y ahí estabas, con tus ojos mirándome fijamente, tendiéndome tu mano e invitándome a tomarla; fue de las pocas veces en mi vida que en mi no hubo ningún titubear, pues la tome al instante, sin duda que cupiera en mi. Y el relámpago no se extinguió y la noche día se tornó mientras las manos seguían unidas y tu boca que para mi lenta se movía me invitaba a no soltarte y caminar junto a ti, y así fue, juntos, caminando hasta que el amanecer me despertó, y lo maldije, porque nunca había deseado tanto no despertar, hasta que apareciste tú mientras dormía. 
