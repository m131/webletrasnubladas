---
layout: post
title: Esperando
excerpt: Aquí estoy, con el corazón temblando, rugiendo, esperando.
author: Mario
category: poema
---

Aquí estoy, con el corazón temblando, rugiendo, esperando. Esperando que quizá tus grandes ojos me pudieran mirar, y al mirarme en ellos poderme reflejar y saber que estoy ahí, en ti. 

Porque es fantasía soñar con verme volar en tu mirada y que tu en la mía te veas reflejada, estrujada por las caricias y abrazos que he guardado tanto tiempo para ti ¡para ti!

Pero aquí estoy, con el corazón temblando, rugiendo, esperando tu mirada.
