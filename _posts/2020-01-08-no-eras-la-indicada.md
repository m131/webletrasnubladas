---
layout: post
title: No eras la indicada
excerpt: No eras la mujer indicada, porque la realidad es que este amor no fue correspondido y las miradas no indicaban nada.
author: Mario
category: poema
---

Quizá no eras la mujer indicada, porque la realidad es que este amor no fue correspondido y las miradas no indicaban nada, quizá no eras la indicada porque dejaste en blanco mis preguntas, les otorgaste una silenciosa respuesta o quizá sólo era tu perezosa indiferencia; quizá no estabas lista o eras demasiado lista para que te bastase tan sólo mi corazón y mi mano. 
 
 Quizá sería mejor arrancar los quizá y aceptar la realidad de lo que llegaste a ser, la sonrisa liberadora que reventó la prisión del alma y cinceló los grilletes del corazón, la realidad de que me liberaste de sombrías tinieblas para andar de nuevo bajo el sol.
