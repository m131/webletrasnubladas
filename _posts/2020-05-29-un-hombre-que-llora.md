---
layout: post
title: Un hombre que llora
excerpt: No hace mucho que en ocasiones ha estado llorando,  pero hacía emasiado tiempo que no sentía sus lagrimas caer sobre su piel.
author: Mario
category: poema
---

No hace mucho que en ocasiones ha estado llorando,  
pero hacía demasiado tiempo que no sentía sus lagrimas caer sobre su piel;  
llorando por sentimientos que lo invaden,  
llorando por ésta vida de la cual no sabe que esperar,  
llora al escribir esto y querer sacar todos esos sentimientos  
confusos que no sabe ni siquiera que son o que significan.  
  
  
Y llora nuevamente al saber que está rodeado de mucha  
gente que lo quiere, que lo aprecian, que lo aman  
y que le dicen y reafirman que es una buena persona,  
que es un buen hombre, pero llora al escuchar sus palabras,  
porque aunque quizá sean ciertas solo es un hombre  
que no sabe que hacer con su vida, un hombre que quisiera encontrar su camino  
y a pesar de estar rodeado de gente que le da su amor se siente tan solo,  
solo es un hombre que llora al sentirse solo aún dentro de una familia,  
llora al no saber como poder decirle que quiere a su madre  
y que quisiera contarle sobre él, un hombre que a veces está harto de ser tan sensible  
y no poder hablar de sus sentimientos o  decirle a una persona que la quiere sin llorar,  
un hombre que casi siempre se siente mejor junto a su perro  
siempre amigo fiel, que junto a las personas; solo es un hombre que llora  
por sueños jamás cumplidos e ilusiones perdidas, alguien que llora  
al escuchar esa vieja canción que habla de amor y esperanza  
porque ya no sabe que esperar de la vida, solo es un hombre que llora  
al platicar con esa siempre fiel amiga que trata de darle ánimos  
pidiéndole que no se rinda ante nada y por nada y que él,  
trata de luchar pero a veces se siente cansado y rendido  
antes de emprender la lucha, solo es un hombre que llora  
al no saber que le depara la vida, porque aún no ha encontrado su camino  
y ni siquiera sabe lo que quiere ser o hacer en ella,  
solo un hombre que llora al ver como la gente a su alrededor  
va encontrando su camino y el simplemente no sabe cual es el de él.  
  
  
Y sigue llorando al sentirse cansado de siempre ayudar a los demás  
y no poder ayudase a si mismo y no saber el como emprender esa lucha interior.  
  
  
Y solo es un hombre que sigue llorando y que trata de escribir todo lo que siente  
y que nuevamente llora al saber que la gente que lo rodea quizá lo mirará  
con lastima o vendrán a él con las mismas palabras de siempre  
queriendo animarlo al leer éstas líneas, sin saber que no hay palabras  
que puedan animar su soledad y menos aún las hay para ayudarle  
a encontrar su camino, porque al final de cuentas solo es un hombre  
que llora por soñar demasiado, por querer creer en la magia, el amor,  
los milagros y por querer ser un héroe, por querer ser mejor para si mismo  
y los demás, un hombre que no puede y no ha logrado encontrarse a si mismo  
en el camino de la vida, alguien que llora por viejas glorias y hazañas ya olvidadas  
y perdidas en el tiempo...  
  
  
Sólo es un hombre que llora al no encontrar su camino en la vida  
y sentirse en soledad.  
