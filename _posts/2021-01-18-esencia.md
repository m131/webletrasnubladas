---
layout: post
title: Esencia
excerpt: Eres un vendaval, una tormenta, un tifón, un huracán, una granizada. Eres cielo despejado, un arco iris, una brisa marina, el canto del viento, la llovizna. 
author: Mario
category: poema
---

Eres un vendaval, una tormenta, un tifón, un huracán, una granizada. Eres cielo despejado, un arco iris, una brisa marina, el canto del viento, la llovizna. Eres el sol, la luna, las constelaciones, todos los astros, los pilares de la creación. Eres fuego, eres agua, eres tierra de donde emana vida, eres corazón. Eres los montes, los valles, los ríos y los mares, los océanos, eres total geografía. Eres deseo, pecado, eres infierno y paraíso. Eres vida, eres muerte, eres la misma esencia de la naturaleza y del universo, eres el todo de quién te suspira y te anhela, el alimento y la inspiración de quién escribe cuando te piensa.  
