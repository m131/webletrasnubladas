---
layout: post
title: Notas inconclusas I
excerpt: Amamos, añoramos y deseamos el tótem de lo que fue...
author: Mario
category: reflexión
---

Amamos, añoramos y deseamos el tótem de lo que fue, la representación viva de algo que ya no existe, de lo que ya no es y ya jamás será, porque las personas cambian, y aunque a veces puede ser para bien, otras tantas son para mal; los buenos recuerdos que guardabas ahora yacen en lo profundo de las cavernas del corazón, con sus entradas tapiadas con mantos de dolor, coraje y decepción.
