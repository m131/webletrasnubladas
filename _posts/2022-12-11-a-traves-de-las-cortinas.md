---
layout: post
title: A través de las cortinas
excerpt: A veces imagino como será ver a través de las cortinas de tu alma, imagino que me permites correrlas para entrar en ti, para vivir en ti.
author: Mario
category: poema
---

A veces imagino como será ver a través de las cortinas de tu alma, 
imagino que me permites correrlas para entrar en ti, para vivir en ti, 
para alimentarme de ti; imagino que un día me invitas a correr esos lienzos 
y que el mundo o yo no somos quienes iluminamos la habitación de tu 
existencia, sino muy al contrario, que es precisamente de ti de donde 
emana esa cálida luz que comienza a darle sentido al mundo, a mi mundo, 
a mi humanidad.
