---
layout: post
title: Pronto llegarás
excerpt: Sé que pronto llegarás, una silueta desconocida entre luces y sombras emergerá.
author: Mario
category: poema
---

Sé que pronto llegarás, una silueta desconocida
entre luces y sombras emergerá y serás tú,
que aún sin nombre, que sin sabernos conocidos y perdidos
en años de historias olvidadas,
encontraremos una misma vereda que andar;
las almas empolvadas se sacudirán
y tú con tu mirada lavarás la mía
y yo con la propia confortaré la tuya.  

Llegarás, llegarás presta como la calma
que le sucede a la tormenta
y nuestras tempestades se apaciguaran;
como se encuentran dos ríos salvajes
nutridos de tormentas, juntos,
juntos desembocaremos en la inmensidad del mar.
