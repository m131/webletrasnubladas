---
layout: post
title: El silencio
excerpt: El silencio también es una respuesta, un tanto cobarde para no hacer ninguna afrenta.
author: Mario
category: poema
---

El silencio también es una respuesta, un tanto cobarde para no hacer ninguna afrenta. El silencio no es ningún arma, y sin embargo a veces hiere e incluso, en ocasiones, mata.
