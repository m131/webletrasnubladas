---
layout: post
title: En minúsculas
excerpt: Te quiero con minúsculas y sin negritas, como quien quiere en silencio, calladamente ahogando sus susurros en un huracán, como una lágrima en medio del mar.
author: Mario
category: poema
---

Te quiero con minúsculas y sin negritas, como quien quiere en silencio, calladamente ahogando sus susurros en un huracán, como una lágrima en medio del mar.
