---
layout: post
title: Frente a mí
excerpt: Ahí estaba yo, al pie de la carretera y justo entre dos árboles que se mesían suavemente al compás del viento y la frescura de la noche mientras escuchaba un eco de mil voces que caminaban.
author: Mario
category: poema
image:  /assets/post-images/ryan-holloway-moon.jpg
---

Ahí estaba yo, al pie de la carretera y justo entre dos árboles que se mecían suavemente al compás del viento y la frescura de la noche, mientras, escuchaba un eco de mil voces que caminaban, y frente a mí estaba ella, alta, radiante, con su dulce cara  blanca que de frente me miraba, que como siempre, como cada mes que ante mi se presentaba, me dejaba perplejo y con el alma hipnotizada.
