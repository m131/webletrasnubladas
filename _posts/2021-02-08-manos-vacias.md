---
layout: post
title: Manos vacías
excerpt: Mis manos están vacías e inertes, mis pies cansados tienen rumbo hacía la nada; la mirada apunta hacía el suelo en señal de una eterna espera, y mi boca está seca y en silencio.
author: Mario
category: poema
---

Mis manos están vacías e inertes, mis pies cansados tienen rumbo hacía la nada; la mirada apunta hacía el suelo en señal de una eterna espera, y mi boca está seca y en silencio.

Mis manos precisan de las tuyas para sentirse vivas y llenas aunque todavía no sepa quien seas , mis pies necesitan caminar junto a ti para darle sentido al camino, eres el cielo que necesito contemplar para levantar la mirada y mi boca necesita de la tuya para humedecerse y gritar al fin libertad.

Y aquí estoy, con las manos vacías, esperando las tuyas para romper la soledad. 
