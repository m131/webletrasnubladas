---
layout: post
title: Despertar
excerpt: Me gusta despertar y ver el rastro de la fugaz lluvia que corrió por la noche...
author: Mario
category: poema
---

Me gusta despertar  
y ver el rastro de la fugaz  
lluvia que corrió por la noche,  
ver un cielo nublado y sentir  
el fresco de la mañana,  
observar como asoma el sol  
tímidamente entre nubes  
grisáceas y blancas,  
respirar el amanecer  
y exhalar el alma.
