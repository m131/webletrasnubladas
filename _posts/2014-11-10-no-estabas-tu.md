---
layout: post
title: No estabas tú
excerpt: Al final no encontré lo que buscaba, no estabas tú, no estaba yo...
author: Mario
category: poema
---

Al final no encontré lo que buscaba,  
no estabas tú, no estaba yo;  
busqué por todas partes,  
bajé cuatro veces al infierno  
y subí alguna vez al cielo,  
me extravíe en incontables desiertos  
y navegué por un par de mares  
y no encontré lo que buscaba;  
lo busqué, en verdad lo busqué  
por todas partes, cientos de veces  
miré con esperanza al cielo  
tan sólo para encontrarlo vacío,  
intenté observar cada estrella  
del negro firmamento, en cada  
cráter de la luna llena y en  
luna nueva buscaba entre sus sombras;  
y al final, no encontré lo que buscaba,  
busqué en tu mirada arrepentida,  
en mis te quiero reprimidos y  
no estabas tú, no estaba yo,   
y definitivamente,  
ya no estábamos los dos.
