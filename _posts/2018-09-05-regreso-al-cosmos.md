---
layout: post
title: Regreso al cosmos
excerpt: En algún inevitable momento ella iba a regresar a su origen, al lugar dónde nació sin haber nacido. 
author: Mario
category: cuento
image:  /assets/post-images/milkyway.jpg
---

Todo en aquel momento era silencio, un silencio fúnebre y desconsolador que era la antesala de lo que estaba sucediendo, y Ana Jímena lo sabía muy bien.  

Ella, como cualquier persona, había tenido una vida buena, una vida mala, una vida al final de cuentas. Ella tenía muy claros sus aciertos, y claro, también sus yerro, de los cuales tenía bastantes de los que arrepentirse, pero, que al final, eran parte de ella y de lo que hasta este momento había sido su vida. Hasta este momento, hasta este momento porque había comenzado a desaparecer, a irse poco a poco, a sentir ese ambiente gélido que dicen se siente cuando ha llegado tu momento. Claro que Ana Jímena estaba confundida y muy temerosa, porque a pesar de ese frío que sentía recorrer su cuerpo, también tenía una extraña sensación de calidez.  

Y aunque habían pasado muchos, muchos años desde que por primera vez vio la luz del mundo, en ese momento no se sentía con ninguna edad, más bien se sentía como todas las edades, se sentía niña, anciana, una adolescente confundida que vaga por la vida, todo al mismo tiempo; con una extraña memoria que ese momento la invadía de todos sus recuerdos a la vez, incluso logró recordar cuando asomó su cabeza a este mundo y ese primer llanto al ser recibida y reconocer los cálidos brazos de su madre que la reconfortarían por primera vez de tantas. Pero ya era hora, el momento era ya inaplazable y había llegado por más que ella quisiera aferrarse al mundo, y con la oscuridad de los ojos cerrados y la sordera de los sollozos y despedidas que escuchaba ella se preguntaba que seguiría.  

Lo que le siguió fue la oscuridad total, el silencio de la paz y el tacto de lo imperceptible, sin frío ni calor, finalmente, había hecho su transición hacia la nada, dónde ya no habitaba ningún recuerdo en su memoria, ningún suspiro de consuelo; aunque sabia que era ella, no recordaba tener nombre alguno, ni edad, ni familia, ni experiencias, no haber tenido una vida.  

Ahora simplemente era una parte del todo, del universo omnisciente, ahora era lluvia, era sol, era tierra, era uno con los gusanos, y con las aves que se alimentaban de ellas y con los animales que se alimentaban de las aves y con las heces de estos y de nuevo era tierra y pasto y árboles con frutos, parte de un todo.  

Al fin, Ana Jímena había regresado a casa, a ser parte de lo que siempre ha sido, esperando en algún momento, de alguna manera, de nuevo ser ella, de nuevo ser vida.
