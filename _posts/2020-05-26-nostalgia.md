---
layout: post
title: Nostalgia
excerpt: La nostalgia no es tristeza ni tampoco alegría, es un punto intermedio.
author: Mario
category: poema
---

La nostalgia no es tristeza  
ni tampoco alegría,   
es un punto intermedio  
entre el desgastado recuerdo  
de la felicidad  
y la añoranza de otra vida.  
La nostalgia no es para los tristes 
 ni para los locos,  
es para quién goza de los recuerdos  
sin poder alcanzarlos  
y en su lucha por alcanzarlos  
de nuevo escriben unos cuantos versos.  
Pero si debiera elegir  
un término para la nostalgia,  
no sería tristeza, no sería felicidad,  
ni siquiera un punto intermedio,  
la nostalgia es allí  
donde tú aún estás.

