---
layout: post
title: Ausente de ti
excerpt: Mi corazón late lento, el tiempo parece no pasar cuando estás ausente, cuando no sé de ti.
author: Mario
category: poema
---

Mi corazón late lento, el tiempo parece no pasar cuando estás ausente, cuando no sé de ti. El día no avanza y estar en la cama pretendiendo no hacer nada no ayuda, aunque en realidad sólo pienso en ti.

Sé que no estás, que el tiempo avanzará  cumpliendo las leyes de relatividad en el espacio. Quizá me quedaré ausente en ti, y yo tendré que buscar avanzar, romper este lento paso del tiempo para poder continuar.
