---
layout: post
title: Pincelada
excerpt: Hoy la luna es una pincelada en el cielo
author: Mario
category: poema
image:  /assets/post-images/moon.jpg
---

Hoy la luna es una pincelada en el cielo, una discreta sonrisa sobre las nubes negras, quizá un atisbo de esperanza para los solitarios, para los necesitados, para los tristes; hoy la luna es una pincelada en el cielo.
