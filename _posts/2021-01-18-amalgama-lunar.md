---
layout: post
title: Amalgama lunar
excerpt: Que la luna llena sea el amalgama restante, que su cósmica luz ilumine almas y trace senderos que culminen en el mismo sitio
author: Mario
category: poema
---

Que la luna llena sea el amalgama restante, que su cósmica luz ilumine almas y trace senderos que culminen en el mismo sitio. Que los ojos y las palabras sean la sigaldria que crea vínculos inquebrantables, que la nominación haga su magia y encuentre el nombre del fuego y el viento para encendernos y avivarnos. Que los anhelos coincidan con la otra parte y sean también anhelados y que los sueños sean soñados, que las dudas y temores sean derrotados y disipados. Y que a la voz del así sea se cumpla lo largamente esperanzado. Así sea. 
