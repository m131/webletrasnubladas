---
layout: post
title: Luna inservible
excerpt: De qué me sirve la luna si no estas conmigo, de que sirve que ilumine mi oscuridad, si no estás tú para guiar mi camino.
author: Mario
category: poema
---

De qué me sirve la luna si no estas conmigo, de que sirve que ilumine mi oscuridad, si no estás tú para guiar mi camino. Porque no importa que esta noche la luna sea llena, cuando lo único que quiero llenar son mis brazos contigo.
