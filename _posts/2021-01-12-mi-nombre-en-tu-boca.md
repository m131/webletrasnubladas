---
layout: post
title: Mi nombre en tu boca
excerpt: Ven, acércate. Pon mi nombre en tu boca y séllalo dentro de ti, nómbrame en tus canciones, piénsame un poco y ponme en tus más fervientes oraciones.
author: Mario
category: poema
---

Ven, acércate. Pon mi nombre en tu boca y séllalo dentro de ti, nómbrame en tus canciones, piénsame un poco y ponme en tus más fervientes oraciones. 

Quiero saber que estoy ahí, contigo, aunque me encuentre ausente. Hazlo, y caminaré a tu lado hasta que este mundo se vuelva cenizas, y aun en la oscuridad por ti estaré. 

Deja que avance hacia ti, invítame a poner tu nombre en mi boca, lo sellaré con mi vida e incluso con mi muerte. Hazlo y nos embarcaremos en un viaje sin final.
