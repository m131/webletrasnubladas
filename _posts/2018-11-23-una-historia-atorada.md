---
layout: post
title: Una historia atorada
excerpt: Tengo una historia atorada en la punta de los dedos...
author: Mario
category: poema
---

Tengo una historia atorada  
en la punta de los dedos,  
la historia de ti, de mí;  
la historia de un loco  
amor de comedia romántica  
y con un tortuoso final  
como el más funesto drama;  
la historia de tu vida, de la mía,  
de dos vidas distintas y  
entrelazadas sin esperarlo  
y atrapadas en un momento  
de un tiempo ya pasado,  
de días extintos sin más amaneceres  
de noches ya sin luna llena  
ni estrellas que titilen;  
tengo una historia atorada  
en la punta de los dedos,  
una historia que no se contar,  
ni siquiera por donde comenzar.  
