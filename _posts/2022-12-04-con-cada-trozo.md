---
layout: post
title: Con cada trozo
excerpt: Te quiero con mis dudas y mis miedos, que no son pocos; te quiero con mis fracasos y mis quebrantos.
author: Mario
category: poema
---
Te quiero con mis dudas y con mis miedos, que no son pocos;  
te quiero con mis fracasos y con mis quebrantos, que son demasiados;  
te quiero con mi tiempo encima, que implacable me marchita;  
te quiero de mil y un maneras, incluso aquellas que ni imaginas;  
te quiero de una manera tan particular que, si me rompiera en mil pedazos, cada trozo te querría aun más.
