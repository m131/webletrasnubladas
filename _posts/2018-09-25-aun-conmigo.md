---
layout: post
title: Aún conmigo
excerpt: Hace meses que te he perdido, te perdí y me dejaste tan sólo con esta extraña calma
author: Mario
category: poema
---

Hace meses que te he perdido, te perdí y me dejaste tan sólo con esta extraña calma que me abraza, que me susurra al corazón que todo está bien, que no hay conflictos entre el cielo y el infierno, que no hay ángel o demonio que rompa esta serenidad.  
    
   
Y respiro profundamente y exhalo ecuanimidad mientras me pregunto dónde estás, y quizá pudiera gritar y rabiar cuestionando tu paradero, pero no hay rastro de ira o tristeza o desesperación. Entonces alzo la mirada sobre estas líneas y comprendo que aún estas conmigo.
