---
layout: post
title: De madrugada
excerpt: Ésta madrugada me dio por correr las persianas...
author: Mario
category: poema
---

Ésta madrugada me dio por correr las persianas y abrir la ventana,   
por dejar que la luna se cuele entre ellas,  
iluminando sobre la cama el lugar que antes ocupabas;  
esta madrugada me dio por aspirar el viento frío  
que mueve las nubes, que viaja entre montañas,  
como esperando poder capturar la avanzada del perfume con el que me abrazabas;  
ésta madrugada me dio por escribirte de nuevo,  
como si no fueras la silueta que hace tiempo se marchó,  
como si tu abundante cabellera aún reposara sobre mí.
