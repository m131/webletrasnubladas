---
layout: post
title: Postergar
excerpt: El tiempo no se detiene y la vida cada día nos acerca más a la muerte, mientras el azar también juega con nosotros y así como hoy estamos, tal vez mañana partamos. 
author: Mario
category: poema
---

El tiempo no se detiene y la vida cada día nos acerca más a la muerte, mientras el azar también juega con nosotros y así como hoy estamos, tal vez mañana partamos. 

Callamos y postergamos cómo si tuviéramos comprado el mañana, como si la muerte nos fuera a esperar hasta que hablemos y hagamos lo aplazado.

Dejamos que los miedos nos dominen como justificación para sobrevivir y callar y postergar; como si eso fuera vivir, como si así estuviéramos comprando la vida y dominando la muerte.
