---
layout: post
title: Notas inconclusas II
excerpt: Es terrible darte cuenta de que sólo fuiste un pretexto para muchas cosas, pero sobre todo, un capricho de placer pasajero...
author: Mario
category: reflexión
---

Es terrible darte cuenta de que sólo fuiste un pretexto para muchas cosas, pero sobre todo, un capricho de placer pasajero; que incrédulamente donde tú viste oportunidad, alguien más sólo vio placer efímero disfrazándolo de añoranza, de nostalgia, de algo que se debía hacer y que al final, tan sólo fue egoísmo puro, todo ello aprovechándose de lo que habitaba en un corazón sincero. Y lo más tonto y quizá debería decir estúpido es, que uno busca darle vueltas a la situación, tratando de justificar palabras y acciones ajenas, cuando en realidad no tienen justificación alguna, e incluso, algunas cosas, ni siquiera tienen perdón.
