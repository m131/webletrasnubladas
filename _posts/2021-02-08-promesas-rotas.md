---
layout: post
title: Promesas rotas
excerpt: Hay un momento que se hacen promesas de no herirse, de no lastimarse. De pronto, un día, hay quién amanece con heridas sangrantes y un corazón que supura tristeza.

author: Mario
category: poema
---

Hay un momento que se hacen promesas de no herirse, de no lastimarse. De pronto, un día, hay quién amanece con heridas sangrantes y un corazón que supura tristeza.

Un día hay canciones para compartir, para dedicar y cantar rabiando de felicidad, al otro sólo queda el inmutable silencio del adiós, del hasta nunca.

Los poemas se desbordaban de los dedos, de la pluma, llenaban estanques de alegría, de pronto, lo que se desborda son lágrimas y desilusión que llenan mares de soledad.

Hay peores cosas que la traición y el engaño, y es la aparición del miedo, y peor que ello, la apatía por miedo. Apatía que logra matar certeramente lo bueno que se llevó por dentro.

¿Y las promesas dónde quedaron? Tiradas en el traspatio, en algún olvidado rincón. 
