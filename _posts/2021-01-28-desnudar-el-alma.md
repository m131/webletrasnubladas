---
layout: post
title: Desnudar el alma
excerpt: Desnudo mi alma para cobijar tu corazón; corazón noble, justo.
author: Mario
category: poema
---

Desnudo mi alma para cobijar tu corazón; corazón noble, justo. Desnudo el alma para mostrarla como es, porque es como has mostrado tu corazón y lo cobijo no porque necesite protección, sino para que sepas que cuando lo necesites, ahí estoy. 
