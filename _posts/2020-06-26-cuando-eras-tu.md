---
layout: post
title: Cuando eras tú
excerpt: Me gustabas cuando eras tú, cuando eras viento y libertad y eras huella que iniciaba sendero.  
author: Mario
category: poema
image:  /assets/post-images/clayton-fidelis-unsplash.jpg
---

Me gustabas cuando eras tú,  
cuando eras viento y libertad  
y eras huella que iniciaba sendero.  

Me gustabas cuando eras tú,  
cuando eras lluvia incesante  
cuando eras alas e inmensidad.  

Me gustabas cuando eras tú,  
antes que tuvieras miedo  
antes de ser como los demás.  

<span>Foto por <a href="https://unsplash.com/@fidelisclayton?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Clayton Fidelis</a> en <a href="/?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>
