---
layout: post
title: Miércoles de ceniza
excerpt: Ella quería que la acompañara a su ritual religioso de "miércoles de ceniza" a pesar de que ella sabía que no soy muy dado a la religión, a pesar de eso, estuvo insistente y acepté ir con ella, al final de cuentas ¿qué podría pasar?, no creo que algo malo y no perdería nada en complacerla.  
author: Mario
category: cuento
image:  /assets/post-images/chica-cuchillo.jpg
---

Ella quería que la acompañara a su ritual religioso de "miércoles de ceniza" a pesar de que ella sabía que no soy muy dado a la religión, a pesar de eso, estuvo insistente y acepté ir con ella, al final de cuentas ¿qué podría pasar?, no creo que algo malo y no perdería nada en complacerla.   

Hicimos fila durante no mucho tiempo, mientras tanto avanzábamos en relativo silencio. Al llegar con el padre hizo lo suyo, dibujar una cruz con ceniza sobre nuestras frentes, nos vio tomados de la mano y sonrío mientras dijo –¡Qué Dios los bendiga!  

Después de eso, caminamos durante un rato rumbo a su casa, en el camino platicamos de cosas que ya no recuerdo, pero nunca olvidaré lo que me susurro al oído al despedirnos: "Fuimos a tomar ceniza para estar bendecidos y protegidos".   

¡Vaya palabras tan absurdas ahora que las recuerdo! porque  nunca dijo protegernos específicamente de qué, porque al final, nada me protegió de ella aquella misma noche cuando me llamó furica para reclamarme de por qué conocía y le hablaba a una chica llamada Mónica, una muy vieja amiga desde la infancia.  

Su reclamo y furia era porque ellas tuvieron una rivalidad hace mucho tiempo, una rivalidad absurda por por alguien más, yo nunca la había visto así. Sus reclamos y gritos eran porque se hacía ideas absurdas acerca de aquella chica y yo, creyendo que ella lo hacía para "joderle la vida", no importó cuanto le explicara que yo la conocía hace años, su ira la cegaba, así que tuve que ir casi a media noche a su casa a hablar con ella.   

Ya estando en su casa la discusión se hizo más física de parte de ella, en verdad no entendía razón alguna, al parecer el conflicto que ellas habían tenido había sido muy grande y ella había quedado muy resentida con eso. Así que no importaba nada más que su repentina desconfianza, a pesar de que siempre le demostré lo inmensamente que la quería, nada, nada importó cuando le dije que mejor me iba, que hablábamos otro día.   
    
Nada importó, por cuando le di la espalda y me dirigí hacía la salida de su cocina, sin que yo lo notara, tomó un cuchillo y me apuñaló por la espalda mientras gritaba:   

– ¡No te vas a ir a ver a esa perra!  

Sentí como toda la piel se me erizaba mientras un intenso dolor recorría toda mi espalda hasta llegar a la nuca. Caí de rodillas y las lágrimas botaron de mis ojos de una forma tan inesperada y dolorosa que incluso hoy, siento ese dolor.   

Todo empezó a verse nublado y tornarse oscuro, pero alcancé a notar que su tono había cambiado y comenzaba a llorar gritándome:   

– ¡perdóname, no quería hacerlo, perdón, perdón, perdóname por favor!  

Fue lo último que recuerdo de ese día, desde entonces llevó en coma 6 años, y cada día tengo la misma pesadilla recurrente acerca de lo que sucedió esa última noche con ella, a veces tengo pequeños momentos de consciencia, como este, donde no sé que día es, porque no puedo moverme o despertar. A pesar de eso, en esos pequeños momentos de consciencia y cuando tengo visitas y me hablan, me siento confortado, aunque sea por instantes, aunque nadie sepa que hay pequeños instantes en que los escucho.   

Pero, a pesar de todo, hay un momento que me aterra, cada miércoles de ceniza cuando ella viene a visitarme.

***

Imagen por [Pexels](https://pixabay.com/es/users/pexels-2286921/) en [Pixabay](https://pixaba.com/)
