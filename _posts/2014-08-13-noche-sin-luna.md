---
layout: post
title: Noche sin luna
excerpt: Si en ésta noche no tengo luna ni tengo tus manos, ¿qué me queda? 
author: Mario
category: poema
---

Si en ésta noche no tengo luna  
ni tengo tus manos,  
¿qué me queda?  
sólo yo con mí basta tristeza,  
sin su luz que me acaricia,  
sin tu abrazo que cobija.  
  
¿Y qué es entonces  
la noche sin luna y sin tus manos?  
es la flor que florece  
marchita en el desierto.

