---
layout: post
title: Mejorar el amor
excerpt: El amor busca mejorar, crecer, respetar, confiar. La pasión sólo es el deseo desmedido haya o no lógica en ello. El amor busca comunión, empatía, solución. La pasión busca satisfacción, desbordarse por placer y no por cariño. 
author: Mario
category: poema
---

El amor busca mejorar, crecer, respetar, confiar. La pasión sólo es el deseo desmedido haya o no lógica en ello. El amor busca comunión, empatía, solución. La pasión busca satisfacción, desbordarse por placer y no por cariño. 

Puede existir amor con pasión, o sin ella; sin ella puede ser un amor consciente o tal vez temeroso, pero amor al fin. Pero no puede haber más pasión que amor, porque entonces no hay respeto y  se busca solo la satisfacción propia, y puede simplemente abandonar y buscar otro objeto de deseo; la pasión sin amor nubla la razón en favor de la satisfacción.

Hay que buscar amor, para crecer, para escuchar, ser escuchado, ser amado, para compartir lo bueno y lo malo. La pasión se puede encontrar en quién sea, pero el amor, el amor rara vez se deja ser encontrado. 
