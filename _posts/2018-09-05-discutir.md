---
layout: post
title: Discutir
excerpt: En estos días de soledad, donde la lluvia me retiene atrapado tras las ventanas
author: Mario
category: poema
---

En estos días de soledad, donde la lluvia me retiene  
atrapado tras las ventanas, me han dado ganas de discutir contigo,  
de saber que hasta en la más absurda discusión estás presente,  
conmigo; de escuchar como tu dulce voz se vuelve amenazante e inquisitiva  
queriendo tener la razón por una tontería,  
todo mientras en silencio, me pierdo en ti,  
pensando en cuanto te quiero ante cualquier situación  
y como hasta cuando explotas en ira eso es parte de ti,  
y así te quiero, en tu plenitud, en tu totalidad.  

En estos días de caminar solo por las aceras y avenidas,  
me han dado ganas de sostener tu mano y sentir como tiras de mí  
para ir presurosa ante un aparador,  
y como ante eso torno la mirada en blanco porque sé que es lo que sigue,  
ver aquí y allá, preguntar por esto y aquello,  
probarte ropa y más ropa sin tener la intención de comprar nada;  
y yo, y yo desesperado por aquella situación,  
pero también sonriente por dentro por estar contigo,  
y por saber que aquello te emociona como a una tierna niña,  
aunque yo de esa emoción no entienda nada.  

Porque en estos días de soledad donde la lluvia y los caminos evocan la nostalgia,  
me dan tantas ganas de estar contigo, de sentirte junto a mi, de simplemente abrazarte,  
pero para poder estar contigo primero, sin sabernos el uno del otro,  
primero, debemos encontrarnos.  
