---
layout: post
title: Gota de mar
excerpt: Gota de mar,  tu salinidad recuerda a las lágrimas que se vierten en la soledad,  eres la figura del vientre que da la vida que rompe en llanto al llegar
author: Mario
category: poema
---

Gota de mar,  tu salinidad recuerda a las lágrimas que se vierten en la soledad,  eres la figura del vientre que da la vida que rompe en llanto al llegar; en tu microscópica figura se encuentran los destellos del sol, los colores del arco iris que se reflejan en su mirar. Gota de mar, que caíste solitaria del cielo, y a pesar de ello, formas parte de la inmensidad, sin ti, el mar no sería mar, y ella no tendría a quién buscar. 
