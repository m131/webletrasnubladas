---
layout: post
title: Árbol de mí
excerpt: Avanza un poco, da unos cuantos pasos y alcanza mis manos y brazos, está  tendidos  como ramas que guarecen a las aves, ellos te darán refugio también.
author: Mario
category: poema
---

Avanza un poco, da unos cuantos pasos y alcanza mis manos y brazos, están tendidos como ramas que guarecen a las aves, ellos te darán refugio también. Y yo seré como el añoso roble, que soporta vientos y huracanes, lluvias y tempestades, seré refugio y hogar, un lugar a donde con seguridad podrás estar.
Vamos, camina hacia mis ramas para  poderte cobijar, entra en el corazón del roble y ve, ve como los años lo fortalecieron para poderte cuidar, como las raíces crecieron profundas para sostenerse y poder darte un lugar que habitar. Seré el roble, el roble que no se vence y donde tus miedos e inseguridades desaparecerán. 
