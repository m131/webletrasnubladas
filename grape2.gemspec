# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "grape2"
  spec.version       = "0.1.0"
  spec.authors       = ["Max131"]
  spec.email         = ["max_cable@hotmail.com"]

  spec.summary       = "Grape theme."
  spec.homepage      = "https://example.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.2"
end
